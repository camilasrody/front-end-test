import Vue from 'vue'
import App from './app.vue'
import router from './router'
import store from './store'
import pageTitle from '@/tools/page-title'
import '@/assets/style.scss'

Vue.mixin(pageTitle)
Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL',
        minimumFractionDigits: 0
    });
    return formatter.format(value);
});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
