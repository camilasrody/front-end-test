module.exports = {
	root: true,
	env: {
		es6: true,
		browser: true,
		es2021: true
	},
	extends: [
		'plugin:vue/essential',
		'airbnb-base',
		'prettier',
	],
	parserOptions: {
		parser: 'babel-eslint',
		ecmaVersion: 12,
		sourceType: 'module',
	},
	plugins: ['vue', 'prettier'],
	rules: {
		'prettier/prettier': 'error',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-unused-vars': 'off',
    'vue/no-unused-components': 'off'
	},
};
